output "eks_oidc_issuer_url" {
  description = "The URL on the EKS cluster OIDC Issuer"
  value       = var.create_eks ? split("//", module.aws_eks.cluster_oidc_issuer_url)[1] : "EKS Cluster not enabled"
}

output "cluster_endpoint" {
  value = var.create_eks ? module.aws_eks.cluster_endpoint : "EKS Cluster not enabled"
}
output "eks_oidc_provider_arn" {
  description = "The ARN of the OIDC Provider if `enable_irsa = true`."
  value       = var.create_eks ? module.aws_eks.oidc_provider_arn : "EKS Cluster not enabled"
}

output "eks_cluster_id" {
  description = "Kubernetes Cluster Name"
  value       = var.create_eks ? module.aws_eks.cluster_id : "EKS Cluster not enabled"
}

output "configure_kubectl" {
  description = "Configure kubectl: make sure you're logged in with the correct AWS profile and run the following command to update your kubeconfig"
  value       = var.create_eks ? "aws eks --region ${data.aws_region.current.id} update-kubeconfig --name ${module.aws_eks.cluster_id}" : "EKS Cluster not enabled"
}

output "cluster_security_group_id" {
  description = "EKS Control Plane Security Group ID"
  value       = module.aws_eks.cluster_security_group_id
}

output "cluster_primary_security_group_id" {
  description = "EKS Cluster Security group ID"
  value       = module.aws_eks.cluster_primary_security_group_id
}