locals {
  eks_oidc_issuer_url = replace(data.aws_eks_cluster.eks_cluster.identity[0].oidc[0].issuer, "https://", "")


  default_managed_ng = {
    node_group_name = "m4_on_demand" # Max node group length is 40 characters; including the node_group_name_prefix random id it's 63
  }

  managed_node_group = merge(
    local.default_managed_ng,
    var.managed_ng
  )

}