variable "eks_cluster_id" {
  type        = string
  description = "EKS Cluster Id"
}

variable "managed_ng" {
  description = "Map of maps of `eks_node_groups` to create"
  type        = any
  default     = {}
}