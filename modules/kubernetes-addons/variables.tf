variable "eks_cluster_id" {
  description = "EKS Cluster Id"
}

variable "eks_oidc_issuer_url" {
  description = "The URL on the EKS cluster OIDC Issuer"
  default     = ""
}

variable "eks_oidc_provider_arn" {
  description = "The ARN of the OIDC Provider if `enable_irsa = true`."
  default     = ""
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit`,`Formac Inc`)"
}


#-----------EKS MANAGED ADD-ONS------------
variable "amazon_eks_vpc_cni_config" {
  description = "ConfigMap of Amazon EKS VPC CNI add-on"
  type        = any
  default     = {}
}

variable "amazon_eks_coredns_config" {
  description = "ConfigMap for Amazon CoreDNS EKS add-on"
  type        = any
  default     = {}
}

variable "amazon_eks_kube_proxy_config" {
  description = "ConfigMap for Amazon EKS Kube-Proxy add-on"
  type        = any
  default     = {}
}

variable "amazon_eks_aws_ebs_csi_driver_config" {
  description = "configMap for AWS EBS CSI Driver add-on"
  type        = any
  default     = {}
}

variable "enable_amazon_eks_vpc_cni" {
  type        = bool
  default     = false
  description = "Enable VPC CNI add-on"
}

variable "enable_amazon_eks_coredns" {
  type        = bool
  default     = false
  description = "Enable CoreDNS add-on"
}

variable "enable_amazon_eks_kube_proxy" {
  type        = bool
  default     = false
  description = "Enable Kube Proxy add-on"
}

variable "enable_amazon_eks_aws_ebs_csi_driver" {
  type        = bool
  default     = false
  description = "Enable EKS Managed AWS EBS CSI Driver add-on"
}


variable "aws_karpenter" {
  description = "ConfigMap of Amazon EKS karpenter"
  type        = any
  default     = {}
}

variable "enable_amazon_eks_aws_karpenter" {
  type        = bool
  default     = false
  description = "Enable karpenter scaling"
}